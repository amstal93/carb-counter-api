<?php

namespace App\Http\Resources;

use App\Http\Resources\NutrionFacts as NutritionFactsResource;
use App\Http\Resources\ExchangeList as ExchangeListResource;

use Illuminate\Http\Resources\Json\JsonResource;

class Food extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'barcode' => $this->barcode->code,
            'category' => $this->category->name,
            
            'nutritionFacts' => new NutritionFactsResource($this->nutritionFacts),
            'exchangeList' => new ExchangeListResource($this->exchangeList),
        ];
    }
}
